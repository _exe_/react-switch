/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{html,jsx,js,tsx}", "./index.html"],
  darkMode: "class",
  theme: {
    extend: {},
  },
  plugins: [],
}

