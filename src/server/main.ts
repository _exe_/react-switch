import express from "express";
import ViteExpress from "vite-express";
import {formatDate, IPStatus, isHostActive} from "./utils.js";
import NodeCache from "node-cache";

const CACHE_KEY = 'hosts-status';
const CACHE_TTL_SECONDS = 600; // 10 minutes

const PORT = process.env.PORT || 3000;
const app = express();
const cache = new NodeCache();


app.use(express.json())

app.get('/api/hosts', async (req, res) => {
    const {host} = req.query;
    if (!host) {
        return res.status(400).send('Request parameter must contain an IP');
    }
    const isActive = await isHostActive(host);
    console.log("/api/hosts/host", host, "ACTIVE", isActive);
    res.status(200).json({ip: host, isActive, updateTime: formatDate(Date.now())})
})

app.post('/api/hosts/all', async (req, res, next) => {
    const {hosts} = req.body;
    const cachedData = cache.get(CACHE_KEY);
    // If cache exists and is still valid, return the cached data
    if (cachedData && (Date.now() - cachedData.timestamp) < CACHE_TTL_SECONDS * 1000) {
        console.log("cached data returns", cachedData);
        return res.json({data: cachedData.results, updatedTime: formatDate(cachedData.timestamp)});
    }

    try {
        if (!hosts || !Array.isArray(hosts)) {
            return res.status(400).send('Request body must contain an array of IP addresses under "hosts"');
        }

        const results: Array<IPStatus> = [];
        for (const host of hosts) {
            const isActive = await isHostActive(host);
            console.log("/api/hosts/all", host, "ACTIVE", isActive);
            results.push({ip: host, isActive});
        }
        res.json({data: results, updatedTime: formatDate(Date.now())});
        cache.set(CACHE_KEY, {timestamp: Date.now(), results});
    } catch (error) {
        next(error);
    }
});


ViteExpress.listen(app, +PORT, () =>
    console.log(`Server is listening on port ${PORT}...`),
);
