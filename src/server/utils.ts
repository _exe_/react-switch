import ping from "ping";

export interface IPStatus {
    ip: string;
    isActive: boolean;
}

export const formatDate = (timestamp: number): string => {
    const date = new Date(timestamp);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return `${day}.${month}.${year} ${hours}:${minutes}`;
};

export const isHostActive = async (ip: string): Promise<boolean> => {
    try {
        const result = await ping.promise.probe(ip, {
            timeout: 10,
        });
        return result.alive;
    } catch (error) {
        if (error instanceof Error && 'code' in error) {
            const typedError = error as { code: string; message: string };
            if (typedError.code === 'ECONNREFUSED') {
                return false;
            }
            throw new Error(`Failed to ping ${ip}: ${typedError.message}`);
        }
        throw new Error(`Failed to ping ${ip}: Unknown error`);
    }
};