import {FC, useContext} from "react";
import Diagram from "./components/Diagram.tsx";
import {useNetwork} from "./useNetwork.ts";
import Loader from "../../components/Loader.tsx";
import {AppContext} from "../../App.tsx";

const Network: FC = () => {
    const {state} = useContext(AppContext);
    const {data: {data, updatedTime}, isLoading} = useNetwork(state.isAutoFetch);
    return (
        <div className="h-full w-full">
            {isLoading && <Loader/>}
            {data.length && <Diagram data={data} updatedTime={updatedTime}/>}
        </div>
    );
};

export default Network;