import {FC, useContext, useEffect, useMemo} from "react";
import {Computer, EdgeType, GraphData, Network, NodeType} from "../model.ts";
import "@react-sigma/core/lib/react-sigma.min.css";
import {SigmaContainer, useLoadGraph, useRegisterEvents, useSigma} from "@react-sigma/core";
import {MultiDirectedGraph} from "graphology";
import {AppContext} from "../../../App.tsx";
import {useFetchHost} from "../useFetchHost.js";
import {createNodeImageProgram} from "@sigma/node-image";

type DiagramProps = {
    data: Network;
    updatedTime: string | null;
};

type GraphProps = {
    data: GraphData;
}

const calculatePositions = (data: GraphData) => {
    const radius = 150;
    const switchNodes = data.nodes.filter((node) => node.type === 'SWITCH');
    const computerNodes = data.nodes.filter((node) => node.type === 'COMPUTER');

    switchNodes.forEach((switchNode, i) => {
        switchNode.x = 500 * i + 100;
        switchNode.y = 300 * i;
        const connectedComputers = data.edges
            .filter((edge) => edge.source === switchNode.id)
            .map((edge) => edge.target);

        connectedComputers.forEach((computerId, j) => {
            const computerNode = computerNodes.find((node) => node.id === computerId);
            if (computerNode) {
                const angle = (2 * Math.PI * j) / connectedComputers.length;
                computerNode.x = switchNode.x! + radius * Math.cos(angle) * 2;
                computerNode.y = switchNode.y! + radius * Math.sin(angle);
            }
        });
    });

    return [...switchNodes, ...computerNodes];
};

const Graph: FC<GraphProps> = ({data}) => {
    const loadGraph = useLoadGraph();
    const registerEvents = useRegisterEvents();
    const sigma = useSigma();
    const [doFetch, {isLoading: hostLoading}] = useFetchHost();
    useEffect(() => {
        const positionedData = calculatePositions(data);
        const graph = new MultiDirectedGraph<NodeType, EdgeType>();
        positionedData.forEach((node) => {
            graph.addNode(node.id, {
                x: node.x,
                y: node.y,
                label: `${node.name} IP: ${node.ip || "відсутнє"}`,
                size: node.type === "SWITCH" ? 20 : 14,
                color: node.isActive === null ? "#94a3b8" : node.isActive ? "green" : "red",
                image: node.type === "SWITCH" ? '/switch.svg' : '/computer.svg'
            });
        })
        data.edges.forEach((link) => {
            graph.addDirectedEdge(link.source, link.target)
        })
        registerEvents({
            enterNode: ({node}) => {
                const host = data.nodes.find(n => n.id === node);
                if (hostLoading) {
                    graph.mergeNodeAttributes(node, {"label": "Завантаження...",})
                    return sigma.refresh();
                }
                if (host?.ip) {
                    const container = sigma.getContainer();
                    if (container) {
                        container.style.cursor = 'pointer';
                    }
                }
            },
            leaveNode: () => {
                const container = sigma.getContainer();
                if (container && container.style.cursor === 'pointer') {
                    container.style.cursor = 'default';
                }
            },
            clickNode: ({node, event}) => {
                const host = data.nodes.find(n => n.id === node);
                if (host?.ip) {
                    graph.mergeNodeAttributes(node, {"label": "Завантаження...",})
                    sigma.refresh();
                    doFetch(host.ip)
                        .then(data => {
                            graph.mergeNodeAttributes(node, {
                                color: data?.isActive ? "green" : "red",
                                label: `${host.name} IP: ${host.ip || ""}`,
                            })
                        })
                        .catch(err => {
                            console.error(err);
                            graph.mergeNodeAttributes(node, {
                                color: host?.isActive ? "green" : "red",
                                label: `${host.name} IP: ${host.ip || ""}`,
                            })
                        })
                        .finally(() => {
                            sigma.refresh();
                        })
                } else {
                    event.preventSigmaDefault();
                }
            }
        });
        loadGraph(graph);
    }, [loadGraph, data, sigma]);

    return null;
};

const Diagram: FC<DiagramProps> = ({data, updatedTime}) => {
    const {state} = useContext(AppContext);
    const settings = useMemo(
        () => ({
            defaultNodeType: 'image',
            nodeProgramClasses: {
                image: createNodeImageProgram({
                    padding: 0.2
                })
            },
            allowInvalidContainer: true,
            defaultEdgeColor: state.mode === "dark" ? "#fff" : "#ccc",
            labelColor: {
                color: state.mode === "dark" ? "#adabab" : "#000",
            }
        }),
        [state],
    );

    const switches = useMemo(() => {
        const nodes = data.flatMap(item => [
            {
                id: item.name,
                name: item.name,
                ip: item.ip,
                type: "SWITCH",
                updateTime: item.updateTime,
                isActive: item.isActive,
                x: 0,
                y: 0
            },
            ...item.computers.map((comp: Computer) => ({
                id: comp.ip,
                ip: comp.ip,
                name: comp.name,
                updateTime: comp.updateTime,
                type: "COMPUTER",
                isActive: comp.isActive,
                x: 0,
                y: 0
            }))
        ]);
        const linkComputersToParent = data.flatMap(item =>
            item.computers.map((comp: Computer) => ({
                source: item.name,
                target: comp.ip
            }))
        );

        const linkSwitches = data.map(item => {
            if (item.link) {
                const linkedSwitch = data.find(switchItem => switchItem.name === item.link);
                if (linkedSwitch && linkedSwitch.link === item.name) {
                    return {source: item.name, target: linkedSwitch.name};
                }
            }
        }).filter(item => item !== undefined);

        return {
            nodes: nodes,
            edges: [...linkComputersToParent, ...linkSwitches].map((item, index) => ({
                ...item,
                id: (index + 1).toString()
            }))
        }
    }, [data]);

    return (
        <SigmaContainer className="h-full w-full dark:bg-gray-900 " settings={settings}>
            <Graph data={switches}/>
            {
                <div
                    className="font-thin text-xs absolute left-0 top-0 bg-gray-300 p-1 z-50 dark:text-gray-300 dark:bg-gray-900">Останнє
                    оновлення: {updatedTime}</div>
            }
        </SigmaContainer>

    );
};

export default Diagram;