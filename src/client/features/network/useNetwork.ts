import {useEffect, useState} from "react";
import {Computer, Network, Switch, SwitchResponse} from "./model.ts";

const processSwitches = (switches: Array<SwitchResponse>, hosts: Array<{ ip: string, isActive: boolean }>): Network => {
    const result: Array<Switch> = [];
    for (const switchObj of switches) {
        const isActive = switchObj.ip ? hosts.find(host => host.ip == switchObj.ip)?.isActive : true;
        const activeComputers: Array<Computer> = switchObj.computers.map(comp => {
            const findActive = hosts.find(host => host.ip === comp.ip).isActive;
            return {...comp, isActive: findActive}
        })
        result.push(<SwitchResponse & { isActive: boolean; computers: Array<Computer> }>{
            ...switchObj,
            isActive,
            computers: activeComputers
        });
    }
    return result;
}

const formatSwitches = (switches: Array<SwitchResponse>): Network => {
    return <Array<Switch>>switches.map((switchItem) => {
        return {
            ...switchItem,
            isActive: null,
            computers: switchItem.computers.map(comp => ({...comp, isActive: null}))
        }
    })
}

const mapIp = (switches: Array<SwitchResponse>): Array<string> => {
    return switches.flatMap(s => {
        return [s.ip as string, ...s.computers.map(comp => comp.ip)].filter(ip => !!ip)
    })
}

export const useNetwork = (isAutoRefetch: boolean) => {
    const [data, setData] = useState<Array<SwitchResponse> | []>([]);
    const [result, setResult] = useState<{ data: Network | [], updatedTime: string | null }>({
        data: [],
        updatedTime: null
    });
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch("/data.json")
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setData(data)
                }
            })
            .catch(error => {
                console.error(error)
            })
    }, []);

    useEffect(() => {
        if (data.length && !result.data.length) {
            setResult((prevState) => ({...prevState, data: formatSwitches(data)}));
        }
        if (data.length) {
            setIsLoading(true);
            const fetchSwitches = async () => {
                try {
                    const fetchPing = await fetch(`/api/hosts/all`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({hosts: mapIp(data)})
                    })
                    const {data: listIP, updatedTime} = await fetchPing.json();
                    const result = processSwitches(data, listIP);
                    setResult({data: result, updatedTime});
                } catch (error) {
                    console.error(error)
                } finally {
                    setIsLoading(false)
                }
            };
            fetchSwitches();

            let intervalId = null;
            if (isAutoRefetch) {
                intervalId = setInterval(fetchSwitches, 600000); //  10 minutes
            } else if (intervalId) {
                clearInterval(intervalId);
            }

            return () => {
                if (intervalId) {
                    clearInterval(intervalId);
                }
            }
        }
    }, [data, isAutoRefetch]);


    return {data: result, isLoading};
}