export type Node = {
    x: number;
    y: number;
    id: string;
    name: string;
    ip: string | null;
    isActive: boolean | null;
    updateTime?: string;
    type: string;
}

export type Edge = {
    id: string;
    source: string;
    target: string;
}


export type GraphData = {
    nodes: Array<Node>;
    edges: Array<Edge>;
}

export interface NodeType {
    x: number;
    y: number;
    label: string;
    size: number;
    color: string;
}

export interface EdgeType {
    type?: string;
    label?: string;
    size?: number;
    curvature?: number;
    parallelIndex?: number;
    parallelMaxIndex?: number;
}

export type Computer = {
    ip: string;
    name: string;
    isActive: boolean | null;
    updateTime?: string;
}

export type SwitchResponse = {
    name: string;
    ip: string | null;
    link: string | null;
    computers: Array<Omit<Computer, "isActive">>;
}

export type Switch = SwitchResponse & {
    isActive: boolean | null;
    computers: Array<Computer>;
    updateTime?: string;
}


export type Network = Array<Switch>



