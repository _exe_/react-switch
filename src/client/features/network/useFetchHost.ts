import {useState} from "react";

export const useFetchHost = () => {
    const [result, setResult] = useState<{ ip: string, isActive: boolean, updateTime: string }>(null);
    const [isLoading, setIsLoading] = useState(false);

    const trigger = async (host: string) => {
        try {
            setIsLoading(true);
            const fetchPing = await fetch(`/api/hosts?host=${host}`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                },
            })
            const data = await fetchPing.json();
            setResult(data);
            return data;
        } catch (error) {
            console.error(error)
        } finally {
            setIsLoading(false)
        }
        return result;
    }

    return [trigger, {data: result, isLoading}]
}