const Loader = () => {
    return (
        <div className='w-full'>
            <div className='h-1 w-full bg-blue-100 dark:bg-blue-950 overflow-hidden'>
                <div className='progress w-full h-full bg-blue-600 left-right'></div>
            </div>
        </div>
    );
};

export default Loader;