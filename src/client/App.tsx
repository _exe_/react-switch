import Network from "./features/network";
import Navbar from "./components/Navbar.tsx";
import {createContext, useEffect, useReducer} from "react";


type AppState = {
    mode: "light" | "dark",
    isAutoFetch: boolean
}
const initialState: AppState = {
    mode: localStorage.getItem("mode") as "light" | "dark" | null || "light",
    isAutoFetch: false
}

export const AppContext = createContext<{ state: AppState; dispatch: any }>({
    state: initialState,
    dispatch: () => {
    }
})

const appReducer = (
    state: AppState,
    action: { payload: any; type: string }
) => {
    switch (action.type) {
        case "SET_THEME":
            localStorage.setItem("mode", action.payload)
            return {
                ...state,
                mode: action.payload
            };
        case "TOGGLE_AUTOREFETCH":
            return {
                ...state,
                isAutoFetch: !state.isAutoFetch
            }
        default:
            return state;
    }
};

function App() {
    const [state, dispatch] = useReducer(appReducer, initialState);

    useEffect(() => {
        if (localStorage.getItem("mode")) {
            document.querySelector("html")?.classList.add(localStorage.getItem("mode") as string);
        }
    }, []);

    return (
        <AppContext.Provider value={{state, dispatch}}>
            <Navbar/>
            <div className="dark:bg-gray-950" style={{height: "calc(100vh - 46px)"}}>
                <Network/>
            </div>
        </AppContext.Provider>
    )
}

export default App
