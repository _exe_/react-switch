# React-Sigma Project

This project utilizes Node.js, React, TypeScript, and React Sigma for data visualization.

## Developer Mode Setup

To get started in developer mode, follow these steps:

Install dependencies:
1. npm install


## Production Mode Setup

For running the project in production mode, use the following commands:

1. Build the project: npm run build
2. Start the production server: npm run start


## Running the Server (if npm run build is already done)

If you have already built the project and need to start the server, use this command:

1. Start the server: npm run start